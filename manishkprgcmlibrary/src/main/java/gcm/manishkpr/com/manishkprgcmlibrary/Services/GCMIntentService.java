package gcm.manishkpr.com.manishkprgcmlibrary.Services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import gcm.manishkpr.com.manishkprgcmlibrary.BroadCasts.GcmBroadcastReceiver;
import gcm.manishkpr.com.manishkprgcmlibrary.Configration.AppConfig;


/**
 * Created by Munish on 5/13/15.
 */
public class GCMIntentService extends IntentService {

	public GCMIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) { }
			else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) { }
			else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				Log.e("GCM Message Received",extras.get(AppConfig.GCM_MESSAGE).toString());
				sendMessageBroadcast(extras.get(AppConfig.GCM_MESSAGE).toString());
			}
		}

		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}
	void sendMessageBroadcast(String message){
		Intent intent = new Intent();
		intent.putExtra(AppConfig.GCM_MESSAGE,message);
		intent.setAction(AppConfig.GCM_BROADCAST_ACTION);
		sendBroadcast(intent);
	}

}
