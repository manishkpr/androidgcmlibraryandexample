package gcm.manishkpr.com.manishkprgcmlibrary.CallBacks;

/**
 * Created by Munish on 5/13/15.
 */
public interface GCMRegisterCallBack {
    void getGCMRegister(String id, boolean isRegister);
}
