package gcm.manishkpr.com.manishkprgcmlibrary.Common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PData {
	 static SharedPreferences m_Prefernce;
	 static SharedPreferences.Editor sp_editor ;

	//## Preference For String
		public static void set_String(Context context,String key,String Value){
			m_Prefernce=PreferenceManager.getDefaultSharedPreferences(context);
			sp_editor = m_Prefernce.edit();
	        sp_editor.putString(key, Value);
	        sp_editor.commit();
		}
		public static String get_String(Context context,String key){
            String str="";
            try {
                m_Prefernce=PreferenceManager.getDefaultSharedPreferences(context);
                str =m_Prefernce.getString(key, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return str;
		}
		//## Preference For Int
		public static void set_Int(Context context,String key,int Value){
			m_Prefernce=PreferenceManager.getDefaultSharedPreferences(context);
			sp_editor = m_Prefernce.edit();
	        sp_editor.putInt(key, Value);
	        sp_editor.commit();
		}
		public static int get_Int(Context context,String key){
			m_Prefernce=PreferenceManager.getDefaultSharedPreferences(context);
			return m_Prefernce.getInt(key,0);
		}
		//## Preference For Boolean
		public static void set_Bool(Context context,String key,boolean Value){
			m_Prefernce=PreferenceManager.getDefaultSharedPreferences(context);
			sp_editor = m_Prefernce.edit();
	        sp_editor.putBoolean(key, Value);
	        sp_editor.commit();
		}
		public static boolean get_Bool(Context context,String key){
			m_Prefernce=PreferenceManager.getDefaultSharedPreferences(context);
			return m_Prefernce.getBoolean(key,false);
				}
		////## Preference to check if exist or not
		public static boolean is_Preference(Context context,String name){
			boolean is_avail=false;
			m_Prefernce=PreferenceManager.getDefaultSharedPreferences(context);
			if(m_Prefernce.contains(name)){
				is_avail=true;
			}else{
				is_avail=false;
			}
			return is_avail;
		}
}
