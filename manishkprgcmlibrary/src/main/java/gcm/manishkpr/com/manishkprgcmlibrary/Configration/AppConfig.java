package gcm.manishkpr.com.manishkprgcmlibrary.Configration;

/**
 * Created by Munish on 5/13/15.
 */
public class AppConfig {

    // Constants
    public static final String GCM_MESSAGE                      =       "gcm_message";
    public static final String GCM_BROADCAST_ACTION             =       "gcm_broadcast_action";
    public static final String IS_REGISTER_GCM                  =       "is_register_gcm";

}
