package gcm.manishkpr.com.manishkprgcmlibrary.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import gcm.manishkpr.com.manishkprgcmlibrary.CallBacks.GCMRegisterCallBack;


/**
 * Created by Munish on 5/13/15.
 */
public class GCMRegister extends AsyncTask<Void,Void,Void> {

    Context context;
    GCMRegisterCallBack callBack;

    GoogleCloudMessaging gcm;
    String gcmDeviceId = "";
    String projectNumber;

    public GCMRegister(Context context,String projectNumber,GCMRegisterCallBack callBack){
            this.context        = context;
            this.callBack       = callBack;
            this.projectNumber  = projectNumber;
    }

    @Override
    protected Void doInBackground(Void... str) {
        if (gcm == null) {
            gcm = GoogleCloudMessaging.getInstance(context);
        }
        try {
            gcmDeviceId = gcm.register(projectNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(gcmDeviceId.equalsIgnoreCase("")) {
            callBack.getGCMRegister(gcmDeviceId,false);
        }else{
            callBack.getGCMRegister(gcmDeviceId,true);
        }

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
