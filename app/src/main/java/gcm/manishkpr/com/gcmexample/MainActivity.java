package gcm.manishkpr.com.gcmexample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import gcm.manishkpr.com.gcmexample.Configration.ContantConfig;
import gcm.manishkpr.com.manishkprgcmlibrary.AsyncTasks.GCMRegister;
import gcm.manishkpr.com.manishkprgcmlibrary.CallBacks.GCMRegisterCallBack;
import gcm.manishkpr.com.manishkprgcmlibrary.Common.PData;
import gcm.manishkpr.com.manishkprgcmlibrary.Configration.AppConfig;

/**
 * Created by Munish on 5/13/15.
 */
public class MainActivity extends BaseActivity {

    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpView();
        registerGCM();
        messageReceiver();
    }

    void setUpView(){
        setContentView(R.layout.activity_main);
        message  = (TextView)findViewById(R.id.message);
        message.append("\n\n");
    }

    void registerGCM(){
        if(!PData.get_Bool(this, AppConfig.IS_REGISTER_GCM))
        new GCMRegister(this, ContantConfig.GOOGLE_CLOUD_PROJECT_NUMBER,callBack).execute();
    }

    GCMRegisterCallBack callBack = new GCMRegisterCallBack() {
        @Override
        public void getGCMRegister(String id, boolean isRegister) {
            if(isRegister)
            PData.set_Bool(MainActivity.this, AppConfig.IS_REGISTER_GCM,isRegister);
            Log.e("ID ",id);
        }
    };

    void messageReceiver(){
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg = intent.getExtras().get(AppConfig.GCM_MESSAGE).toString();
                message.append(msg+"\n");
            }
        };
    }
}
