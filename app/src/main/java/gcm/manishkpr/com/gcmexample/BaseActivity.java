package gcm.manishkpr.com.gcmexample;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;

import gcm.manishkpr.com.manishkprgcmlibrary.Configration.AppConfig;

/**
 * Created by Munish on 5/14/15.
 */
public class BaseActivity extends Activity {

    public BroadcastReceiver broadcastReceiver;

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(AppConfig.GCM_BROADCAST_ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

}
