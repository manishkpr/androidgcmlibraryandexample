# Introduction #

This Library is developed for using Google Cloud Messaging For Android.

# Installation Server PHP  #

File gcm.php


```
#!php

<?php
	     //## Function to send GCM push notification
         function sendMessageUsingGCM($registatoin_ids, $message) {

        //## Google cloud messaging GCM-API url

        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array( 'registration_ids' => $registatoin_ids, 'data' => $message,);

	    //## Update your Google Cloud Messaging API Key

	    define("GOOGLE_API_KEY", "Place Google Api Key Here"); 		
        $headers = array('Authorization: key=' . GOOGLE_API_KEY,'Content-Type: application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;

    }

	$id = 'device_id';//### Place Device ID here
	$gcmRegIds = array($id);
	$message    = $_REQUEST['message'];
	$message    = array("gcm_message" => $message);
	$pushStatus = sendMessageUsingGCM($gcmRegIds, $message);
	echo $pushStatus;

?>
```

Test Example : http://yoursite.com/gcm.php?message=hello


# Installation Client #

**Step 1:** 

place 'manishkprgcmlibrary' downloaded library inside you project root directory 

**Step 2:**

Add the followings dependencies to you build.gradle file


```
#!JSON

dependencies {
    compile project(':manishkprgcmlibrary')
    compile 'com.google.android.gms:play-services:+'
}

```

**Step 3:** 

Add following permissions to your AndroidManifest.xml File

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.GET_ACCOUNTS" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.VIBRATE" />

    <!-- Create Custom Permission 'your.package.name' replace with your project package  -->
    <permission android:name="your.package.name.permission.C2D_MESSAGE" android:protectionLevel="signature" />

    <!-- GCM Permissions Start here  -->

    <uses-permission android:name="your.package.name.permission.C2D_MESSAGE" />
    <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

**Step 4:** 

Add Broadcast and Service to your AndroidManifest.xml File

 <!-- Register Broadcast receiver -->

        <receiver
            android:name="gcm.manishkpr.com.manishkprgcmlibrary.BroadCasts.GcmBroadcastReceiver"
            android:permission="com.google.android.c2dm.permission.SEND" >
            <intent-filter>
                <action android:name="com.google.android.c2dm.intent.RECEIVE" />
                <action android:name="com.google.android.c2dm.intent.REGISTRATION" />

                <category android:name="your.package.name" /> <!-- replace it with your package name -->

            </intent-filter>
        </receiver>

 <!-- Register Service -->

        <service android:name="gcm.manishkpr.com.manishkprgcmlibrary.Services.GCMIntentService" />

**Step 5:**

//### For Registering your device


```
#!java

new GCMRegister(this, "Your Project Number",callBack).execute(); //## ForActivity 
new GCMRegister(getActivity(), "Your Project Number",callBack).execute(); //## For Fragment

//### You Can Get GCM ID By following Call Back

GCMRegisterCallBack callBack = new GCMRegisterCallBack() {
        @Override
        public void getGCMRegister(String id, boolean isRegister) {
            if(isRegister)
            Log.i("ID ",id);
        }
};
```

**Step 6:**

For Receiving Messages Add Broadcast to your Fragment or Activity

   
   
```
#!java

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg = intent.getExtras().get(AppConfig.GCM_MESSAGE).toString();
                Log.i("Message Received ",msg);
            }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(AppConfig.GCM_BROADCAST_ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

```

That's It

For More Information See The Example Inside This Library

Or 

Visit: http://manishkpr.webheavens.com